import { shallowMount } from '@vue/test-utils';
import { nextTick } from 'vue';
import DRadio from '@/components/DRadio.vue';

describe('radio', () => {
  let wrapper = null;
  const createComponent = (options) => {
    wrapper = shallowMount(DRadio, options);
  };

  afterEach(() => {
    if (wrapper) {
      wrapper.unmount();
      wrapper = null;
    }
  });

  it('renders input[type="radio"] inside', () => {
    createComponent({ props: { value: {} } });

    expect(wrapper.find('input[type="radio"]').exists()).toBe(true);
  });

  it('renders option text inside', () => {
    const VALUE = { id: 1 };
    const LABEL = 'Опция 1';
    createComponent({
      props: {
        value: VALUE,
        label: LABEL,
      },
    });

    expect(wrapper.text()).toContain(LABEL);
  });

  it("renders unchecked input if model isn't equal to value", async () => {
    const MODEL = {};
    const VALUE = {};
    createComponent({
      props: {
        modelValue: MODEL,
        value: VALUE,
      },
    });
    await nextTick();

    expect(wrapper.find('input:not(:checked)').exists()).toBe(true);
  });

  it('renders checked input if model is equal to value', async () => {
    const MODEL = { id: 1 };
    createComponent({
      props: {
        modelValue: MODEL,
        value: MODEL,
      },
    });
    await nextTick();

    expect(wrapper.find('input:checked').exists()).toBe(true);
  });

  it('emits update:modelValue after click on wrapper', async () => {
    createComponent({
      props: {
        modelValue: {},
        value: {},
      },
    });

    await wrapper.trigger('click');

    const emitted = wrapper.emitted();
    const emittedModelUpdates = emitted['update:modelValue'];

    expect(emittedModelUpdates).toHaveLength(1);
  });

  it('emits correct value', async () => {
    const MODEL = {};
    const VALUE = { id: 1 };
    createComponent({
      props: {
        modelValue: MODEL,
        value: VALUE,
      },
    });
    await wrapper.trigger('click');

    const emittedModelUpdates = wrapper.emitted()['update:modelValue'];
    expect(emittedModelUpdates).toHaveLength(1);
    expect(emittedModelUpdates[0][0]).toEqual(VALUE);
  });

  it('changes checked state when modelValue updates', async () => {
    createComponent({
      props: {
        modelValue: {},
        value: { id: 1 },
      },
    });
    expect(wrapper.find('input:not(:checked)').exists()).toBe(true);

    await wrapper.trigger('click');
    const emittedModelUpdates = wrapper.emitted()['update:modelValue'];
    const emittedValue = emittedModelUpdates[0][0];
    await wrapper.setProps({ modelValue: emittedValue });
    await nextTick();

    expect(wrapper.find('input:checked').exists()).toBe(true);
  });

  it('renders disabled input', async () => {
    createComponent({
      attrs: { disabled: true },
      props: {
        modelValue: {},
        value: {},
      },
    });

    await nextTick();

    expect(wrapper.find('input:disabled').exists()).toBe(true);
  });

  it('doesn\'t emit update:modelValue if disabled', async () => {
    createComponent({
      attrs: { disabled: true },
      props: {
        modelValue: {},
        value: {},
      },
    });
    await nextTick();
    expect(wrapper.find('input:disabled').exists()).toBe(true);

    await wrapper.trigger('click');

    expect(wrapper.emitted()['update:modelValue']).toBe(undefined);
  });
});
