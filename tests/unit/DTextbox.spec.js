import { shallowMount } from '@vue/test-utils';
import DTextbox from '@/components/DTextbox.vue';

describe('textbox', () => {
  let wrapper = null;
  const createComponent = (options) => {
    wrapper = shallowMount(DTextbox, options);
  };

  const getTextInput = () => wrapper.find('input[type="text"]');

  afterEach(() => {
    if (wrapper) {
      wrapper.unmount();
      wrapper = null;
    }
  });

  it('renders input type=text by default', () => {
    createComponent();

    expect(getTextInput().exists()).toBe(true);
  });

  it('renders text in default slot', () => {
    const TEXT = 'Введите текст';
    createComponent({
      slots: {
        default: TEXT,
      },
    });

    expect(wrapper.text()).toContain(TEXT);
  });

  it('sets modelValue as input value', () => {
    const TEXT = 'Введите текст';
    createComponent({
      props: {
        modelValue: TEXT,
      },
    });
    expect(getTextInput().exists()).toBe(true);

    // eslint-disable-next-line no-underscore-dangle
    expect(getTextInput().wrapperElement._value).toBe(TEXT);
  });

  // TODO: implement
  // it('sets focus on input after click on wrapper');

  // it('emits update:modelValue with user input');

  it('renders disabled input', () => {
    createComponent({
      props: { disabled: true },
    });

    expect(wrapper.find('input:disabled').exists()).toBe(true);
  });

  it('renders readonly input', () => {
    createComponent({
      props: { readonly: true },
    });

    expect(wrapper.find('input[readonly]').exists()).toBe(true);
  });
});
