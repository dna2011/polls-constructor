import { shallowMount } from '@vue/test-utils';
import DCheckbox from '@/components/DCheckbox.vue';

describe('DCheckbox', () => {
  let wrapper;
  const INPUT_CHECKED_SELECTOR = 'input:checked';
  const INPUT_NOT_CHECKED_SELECTOR = 'input:not(:checked)';
  const createComponent = (options) => {
    wrapper = shallowMount(DCheckbox, options);
  };

  afterEach(() => {
    if (wrapper) {
      wrapper.unmount();
      wrapper = null;
    }
  });

  it('renders input type=checkbox', () => {
    createComponent({ props: { value: {} } });

    expect(wrapper.find('input[type="checkbox"]').exists()).toBe(true);
  });

  it('renders text from label prop', () => {
    const LABEL = 'Option X';
    createComponent({
      props: {
        value: {},
        label: LABEL,
      },
    });

    expect(wrapper.text()).toContain(LABEL);
  });

  it('renders unchecked input', () => {
    createComponent({
      props: {
        value: {},
      },
    });

    expect(wrapper.find(INPUT_NOT_CHECKED_SELECTOR).exists()).toBe(true);
  });

  it('renders unchecked input when different modelValue provided', () => {
    createComponent({
      props: {
        modelValue: [{}],
        value: {},
      },
    });

    expect(wrapper.find(INPUT_NOT_CHECKED_SELECTOR));
  });

  it('renders checked input when same modelValue provided', () => {
    const VALUE = {};
    createComponent({
      props: {
        modelValue: [VALUE],
        value: VALUE,
      },
    });

    expect(wrapper.find(INPUT_CHECKED_SELECTOR).exists()).toBe(true);
  });

  it('emits update:modelValue after click on wrapper', async () => {
    createComponent({
      props: {
        modelValue: [],
        value: { id: 1 },
      },
    });

    await wrapper.trigger('click');

    expect(wrapper.emitted()['update:modelValue']).toBeDefined();
    expect(wrapper.emitted()['update:modelValue']).toHaveLength(1);
  });

  it('emits update:modelValue with correct payload', async () => {
    const VALUE = { id: 1 };
    createComponent({
      props: {
        modelValue: [],
        value: VALUE,
      },
    });

    await wrapper.trigger('click');

    const emittedUpdates = wrapper.emitted()['update:modelValue'];
    expect(emittedUpdates).toBeDefined();
    expect(emittedUpdates).toHaveLength(1);
    expect(emittedUpdates[0][0][0]).toEqual(VALUE);
  });

  it('updates input state after click', async () => {
    const VALUE = { id: 1 };
    createComponent({
      props: {
        modelValue: [],
        value: VALUE,
      },
    });
    expect(wrapper.find(INPUT_NOT_CHECKED_SELECTOR).exists()).toBe(true);

    await wrapper.trigger('click');

    expect(wrapper.find(INPUT_CHECKED_SELECTOR).exists()).toBe(true);
  });

  it('updates input state when modelValue changes', async () => {
    const VALUE = { id: 1 };
    createComponent({
      props: {
        modelValue: [],
        value: VALUE,
      },
    });
    expect(wrapper.find(INPUT_NOT_CHECKED_SELECTOR).exists()).toBe(true);

    await wrapper.setProps({ modelValue: [VALUE] });

    expect(wrapper.find(INPUT_CHECKED_SELECTOR).exists()).toBe(true);
  });

  it('renders disabled input if attribute is provided', () => {
    createComponent({
      attrs: { disabled: true },
      props: { modelValue: [], value: { id: 1 } },
    });

    expect(wrapper.find('input:disabled').exists()).toBe(true);
  });

  it('doesn\'t emit update:modeValue if disabled', async () => {
    createComponent({
      attrs: { disabled: true },
      props: { value: { id: 1 }, modelValue: [] },
    });
    expect(wrapper.find('input:disabled').exists()).toBe(true);

    await wrapper.trigger('click');

    expect(wrapper.emitted()['update:modelValue']).toBeUndefined();
  });
});
